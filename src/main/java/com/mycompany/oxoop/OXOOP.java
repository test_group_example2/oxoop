/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mycompany.oxoop;

import java.util.Scanner;

/**
 *
 * @author Kitty
 */
public class OXOOP {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        Game game = new Game();
        game.showWelcome();
        game.newBoard();

        while (true) {
            game.showTable();
            game.showTurn();
            game.inputRowCol();
            if (game.isFinish()) {
                game.showTable();
                game.showResult();
                game.showStat();
                if (CheckPlayAgain(kb, game)) {
                    break;
                }
            }
        }
    }

    private static boolean CheckPlayAgain(Scanner kb, Game game) {
        System.out.print("Do you want to play again? (y/n): ");
        char x = kb.next().charAt(0);
        if (x == 'y') {
            System.out.println();
            game.newBoard();
        } else {
            return true;
        }
        return false;
    }
}
