/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.oxoop;

import java.io.Serializable;

/**
 *
 * @author Kitty
 */
public class Player implements Serializable{

    private char symbol;
    private int win;
    private int loss;
    private int draw;

    public Player(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    public void win() {
        this.win++;
    }

    public void loss() {
        this.loss++;
    }

    @Override
    public String toString() {
        return "win: " + win + " loss: " + loss + " draw: " + draw;
    }

    public void draw() {
        this.draw++;
    }
}
